import {SIGN_UP} from "./ActionType";

const userRegister = new UserRegister();

const reduxAction = (type, payload) => {
    return {
        type: type,
        payload: payload,
    }
}
