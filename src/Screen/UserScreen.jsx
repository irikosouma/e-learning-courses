import React, { Component } from 'react'
import HeaderUS from "../Layout/UserScreens/HeaderUS"; 
import FancyHeader from '../Layout/FancyHeader';
import HeaderUser from '../Layout/UserScreens/HeaderUser';
import SliderList from '../Layout/SliderList/SliderList';
import MarketingCourse from '../Layout/SliderList/MarketingCourse';
export default class UserScreen extends Component {
    render() {
        return (
            <div>
                <FancyHeader/>
                <HeaderUS/>
                <HeaderUser/>
                <div style={{textAlign: 'center', fontFamily: 'Times New Roman', fontSize: '25px'}}>Registed Course</div>
                <SliderList/>
                <SliderList/>
                <SliderList/>
                <div style={{textAlign: 'center', fontFamily: 'Times New Roman', fontSize: '25px'}}>Course in cart</div>
                <SliderList/>
                <SliderList/>
                <SliderList/>
                <MarketingCourse/>
            </div>
        )
    }
}
