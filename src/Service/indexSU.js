import axios from "axios"
import { settings } from "../Config/setting"

export const restConnector = axios.create({
    baseURL: settings.domain
})