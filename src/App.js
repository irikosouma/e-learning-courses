import React from "react";
// import Login from "./Components/LogRes/Login";
// import Header from "./Layout/Header";
// import FancyHeader from "./Layout/FancyHeader";
// import Slider from "./Layout/Slider";
// import HeaderUs from "./Layout/UserScreens/HeaderUS";
// import SliderItem from "./Layout/CourseSliders/SliderItem";
// import CourseSlider from "./Layout/CourseSliders/CourseSlider";
import {connect} from 'react-redux';
import { fetchAllUser } from "./Reducers/ActionReducer";
import AppRoot from "./v2/screens/AppRoot";

class App extends React.Component {
  render() {
    return (
      // <>
      //   <FancyHeader />
      //   <Header />
      //   {/* < HeaderUs/> */}
      //   <Slider />
      //   {/* <CourseItem/> */}
      //   {/* <SliderItem/> */}
      //   <CourseSlider />
      // </>
      <AppRoot/>
    );
  }

  componentDidMount(){
    this.props.dispatch(fetchAllUser());
  }
}

const mapStateToProps = (state)=>{
  console.log(state);
  return {
    users: state.user.users
  }
}

export default connect(mapStateToProps)(App);
