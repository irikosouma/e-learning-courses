import Axios from "axios";

class UserService {
  signUp(chikhung) {
    return Axios({
      method: "POST",
      url: "http://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
      data: chikhung
    });
  }

  getAllUser(){
    return Axios({
      method: "GET",
      url: "http://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachNguoiDung",
    });
  }
  
}
export default UserService;
