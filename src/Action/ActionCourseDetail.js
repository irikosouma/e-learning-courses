import { FETCH_COURSE_DETAIL } from "../Type/ActionType";
import CourseService from "../Service/courseService";

const courseService = new CourseService();

const reduxAction = (type, payload) => {
    return {
        type: type,
        payload: payload
    };
};

export const fetchCourseDetail = (courseId) => {
    return dispatch => {
        courseService
        .fetchCourseDetail(courseId)
        .then(res => {
            dispatch(reduxAction(FETCH_COURSE_DETAIL, res.data));
        })
        .catch(err => {
            console.log(err)
        });
    };
};

export default reduxAction;