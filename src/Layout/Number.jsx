import React, { Component } from "react";
import "./number.scss";

export default class Number extends Component {
  render() {
    return (
     <div class="number container-fluid" style={{ height: '130px', color: 'white', alignItems: 'center'}}>
  <section className="number row" style={{textAlign: 'center'}}>
    <div className="number__items col-3">
      <p className="counter" style={{fontSize:'30px'}}>12345</p>
      <p className="p2">Expert instruction</p>
    </div>
    <div className="number__items col-3">
      <p className="counter p1" style={{fontSize:'30px'}}>100.000</p>
      <p className="p2"  style={{fontSize:'20px'}}>Online Courses</p>
    </div>
    <div className="number__items col-3">
      <p className="counter"  style={{fontSize:'30px'}}>1.000.000.000</p>
      <p className="p2" style={{fontSize:'20px'}}>Workwide memberships</p>
    </div>
    <div className="number__items col-3">
      <p className="counter"  style={{fontSize:'30px'}}>100</p>
      <p className="p2" style={{fontSize:'20px'}}>Global corporations</p>
    </div>
  </section>
</div>

    );
  }
}
