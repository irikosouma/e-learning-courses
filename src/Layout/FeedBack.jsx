import React, { Component } from 'react'
import "./FeedBack.scss";
export default class FeedBack extends Component {
    render() {
        return (
            <div>
  <section className="team">
    <h1 className="team__heading">Team Testimonial</h1>
    <div className="team__content">
      <div className="team__items">
        <img src="Css/img/h1.jpg" alt />
        <div className="team_textPink">
          <p>John Simon</p>
          <p>Cake Maker</p>
        </div>
        <div className="team__textBlack">
          <p>John Simon</p>
          <p>-Cake Maker</p>
          <p>Mauris tincidunt dolor eget diam dapibus vitae finibusnisl friuisque pretiuam</p>
          <div>
            <i className="fab fa-facebook-f" />
            <i className="fab fa-google-plus-g" />
            <i className="fab fa-twitter" />
          </div>
        </div>
      </div>
      <div className="team__items team__items2">
        <img src="Css/img/h1.jpg" alt />
        <div className="team_textPink">
          <p>Max Will</p>
          <p>Coffee Maker</p>
        </div>
        <div className="team__textBlack">
          <p>Max Will</p>
          <p>-Coffee Maker</p>
          <p>Mauris tincidunt dolor eget diam dapibus vitae finibusnisl friuisque pretiuam</p>
          <div>
            <i className="fab fa-facebook-f" />
            <i className="fab fa-google-plus-g" />
            <i className="fab fa-twitter" />
          </div>
        </div>
      </div>
      <div className="team__items">
        <img src="Css/img/h1.jpg" alt />
        <div className="team_textPink">
          <p>Rose Murphy</p>
          <p>Staff Manager</p>
        </div>
        <div className="team__textBlack">
          <p>Rose Murphy</p>
          <p>-Staff Manager</p>
          <p>Mauris tincidunt dolor eget diam dapibus vitae finibusnisl friuisque pretiuam</p>
          <div>
            <i className="fab fa-facebook-f" />
            <i className="fab fa-google-plus-g" />
            <i className="fab fa-twitter" />
          </div>
        </div>
      </div>
      <div className="team__items">
        <img src="Css/img/h1.jpg" alt />
        <div className="team_textPink">
          <p>Barbara Will</p>
          <p>Cake Maker</p>
        </div>
        <div className="team__textBlack">
          <p>Barbara Will</p>
          <p>-Cake Maker</p>
          <p>Mauris tincidunt dolor eget diam dapibus vitae finibusnisl friuisque pretiuam</p>
          <div>
            <i className="fab fa-facebook-f" />
            <i className="fab fa-google-plus-g" />
            <i className="fab fa-twitter" />
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

        )
    }
}
