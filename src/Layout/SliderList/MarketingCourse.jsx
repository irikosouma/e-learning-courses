import React, { Component } from 'react'
import SliderItem from '../../CourseItems/SliderItem'

export default class MarketingCourse extends Component {
    render() {
        return (
            <div>
                <div className="container">
                <div className="row">
                    <div className="bg-dark col-3" style={{ width: '298px', height: '338px', alignItems: 'center' }}>
                        <p className="text-white text-small text-center">The world’s largest selection of courses. Choose from over 100,000 online video courses with new additions published every month</p>
                    </div>
                    <div className="col-9">
                    <div className="row">
                        <button type="button" className="btn btn-link text-dark  mr-5 ml-1">Backend</button>
                        <button type="button" className="btn btn-link text-dark mr-5" >Design</button>
                        <button type="button" className="btn btn-link text-dark mr-5" >Mobil App</button>
                        <button type="button" className="btn btn-link text-dark mr-5" >Frontend</button>
                        <button type="button" className="btn btn-link text-dark mr-4" >Full Stack</button>
                        <button type="button" className="btn btn-link text-dark" >Coding Orientation</button>                       
                    </div>  
                    <hr width="855px" align="center" color="grey" ></hr>
                     <div className="row">
                     <div className="col-3">
                        <SliderItem></SliderItem>
                        
                    </div>
                    <div className="col-3"> 
                        <SliderItem></SliderItem>
                        
                    </div>
                    <div className="col-3">
                        <SliderItem></SliderItem>
                        
                    </div>
                    <div className="col-3">
                        <SliderItem></SliderItem>
                        
                    </div>
                    </div> 
                    
                    </div>
                    

                </div>

            </div>
            </div>
        )
    }
}
