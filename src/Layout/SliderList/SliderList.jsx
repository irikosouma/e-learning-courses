import React, { Component } from "react";

export default class SliderList extends Component {
  render() {
    return ( 
        <div
          className="card container"
          style={{ width: "894px", height: "100px" }}
        >
          <div className="row">
            <div className="col-3">
              <img
                className="card-img"
                src="/img/image.png"
                alt=""
                style={{ width: "168px", height: "91px" }}
              ></img>
            </div>
            <div className="col-6 ">
              <p className="text small" style={{fontFamily: 'Times New Roman'}}>Course name</p>
              <p className="text small" style={{fontFamily: 'Times New Roman'}}>Description</p>
            </div>
            <div className="col-3 text-primary">
              <p className="text small" style={{fontFamily: 'Times New Roman'}}>Remove</p>
              <p className="text small" style={{fontFamily: 'Times New Roman'}}>Edit</p>
              <p className="text small" style={{fontFamily: 'Times New Roman'}}>Delete</p>
            </div>
          </div>
        </div>
    );
  }
}
