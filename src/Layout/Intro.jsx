import React, { Component } from "react";
import "./FeedBack.scss";
export default class Intro extends Component {
  render() {
    return (
      <div class="container">
        <p
          class="row"
          style={{ fontSize: "30px", fontFamily: "Times New Roman" }}
        >
          "Choose from 100.000 online video courses with new additions published
          every month"
        </p>
        <section className="team">
          <h1 className="team__heading">Team Testimonial</h1>
          <div className="team__content row">
            <div className="team__items">
              <img src="../../public/img/h1.jpg" alt />
              <p>John Simon</p>
              <p>Good</p>
              <p>
                Mauris tincidunt dolor eget diam dapibus vitae finibusnisl
                friuisque pretiuam
              </p>
              <div>
                <i className="fab fa-facebook-f" />
                <i className="fab fa-google-plus-g" />
                <i className="fab fa-twitter" />
              </div>
            </div>
            <div className="team__items">
              <img src="../../public/img/h1.jpg" alt />
              <p>John Simon</p>
              <p>Good</p>
              <p>
                Mauris tincidunt dolor eget diam dapibus vitae finibusnisl
                friuisque pretiuam
              </p>
              <div>
                <i className="fab fa-facebook-f" />
                <i className="fab fa-google-plus-g" />
                <i className="fab fa-twitter" />
              </div>
            </div>
            <div className="team__items">
              <img src="../../public/img/h1.jpg" alt />
              <p>John Simon</p>
              <p>Good</p>
              <p>
                Mauris tincidunt dolor eget diam dapibus vitae finibusnisl
                friuisque pretiuam
              </p>
              <div>
                <i className="fab fa-facebook-f" />
                <i className="fab fa-google-plus-g" />
                <i className="fab fa-twitter" />
              </div>
            </div>
            <div className="team__items">
              <img src="../../public/img/h1.jpg" alt />
              <p>John Simon</p>
              <p>Good</p>
              <p>
                Mauris tincidunt dolor eget diam dapibus vitae finibusnisl
                friuisque pretiuam
              </p>
              <div>
                <i className="fab fa-facebook-f" />
                <i className="fab fa-google-plus-g" />
                <i className="fab fa-twitter" />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
