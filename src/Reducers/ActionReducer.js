import UserService from "../Services/user"
import ConstReducer from "./ConstReducer";
const userService = new UserService()
export const actionType = (type, payload) => {
  return {type, payload}
}
export const fetchAllUser = () => {
  return dispatch => {
    userService.getAllUser().then(res => {
      const {data} = res;
      dispatch(actionType(ConstReducer.USER_GET, data))
    })
  }
}