import ConstReducer from "../ConstReducer";

const { USER_LOGIN, USER_ADD, USER_DEL, USER_GET } = ConstReducer;
const initState = {
  userLogin: {},
  users: []
};

export const UserReducer = (state = initState, {type, payload}) => {
  switch (type) {
    case USER_GET: {
      state.users = payload;
      return {...state}
    }
    
    default:
      return state;
  }
};
