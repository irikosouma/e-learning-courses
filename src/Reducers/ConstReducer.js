const ConstReducer = {
  // user
  USER_LOGIN: 'USER_LOGIN',
  USER_ADD: 'USER_ADD',
  USER_DEL: 'USER_DEL',
  USER_GET: 'USER_GET',
  // course
  COURSE_ADD: 'COURSE_ADD'
}

export default ConstReducer