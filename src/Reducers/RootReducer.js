import { combineReducers } from "redux";
import { UserReducer } from "./reducer/UserResucer";

const RootReducer = combineReducers({
    user: UserReducer
});

export default RootReducer;
