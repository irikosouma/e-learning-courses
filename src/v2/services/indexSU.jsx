import axios from "axios"
import { settings } from "../redux/config/setting"

export const restConnector = axios.create({
    baseURL: settings.domain
})