
import axios from 'axios';
import { settings } from '../redux/config/setting';

const restConnector = axios.create({
    baseURL: settings.domain
        
});
export default restConnector;