import React, { Component } from 'react';


class CourseItem extends Component {
    render() {
        return (
            <div className="card container" style={{ width:'894px', height:'100px'}}>
                <div className="row">
                <div className="col-3">
                    <img className="card-img" scr="/img/image.jpg" alt="" style={{ width:'168px', height:'99px' }}></img>
                </div>
                <div className="col-6 ">
                    <p className="text small">Course name</p>
                    <p className="text small">Description</p>
                </div>
                <div className="col-3 text-primary">
                    <p className="text small">Remove</p>
                    <p className="text small">Edit</p>
                    <p className="text small">Delete</p>
                </div>
                </div>              
            </div>
        )
    }
}
export default CourseItem;
