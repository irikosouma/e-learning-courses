import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { userLoginAction } from '../../redux/UserAction/UserAction';

function Login(props) {
    let [state, setState] = useState({
        useLogin: {
            taiKhoan: '',
            matKhau: ''
        },
        errors: {
            taiKhoan: '',
            matKhau: ''
        }

    }) 

    let handleChange = (e) => { 
        let { name, value } = e.target;
        let errorMessage = " ";

        if (value === "") {
            errorMessage = name + 'is required';
        }

        setState({
            userLogin: { ...state.userLogin, [name]: value },
            errors: { ...state.errors, [name]: errorMessage }
        });
        console.log(e.target.value);
        
    }


    let handleSubmit = (e) => {
        e.preventDefault(); 
        let valid = true
        for (var errorName in state.errors) {
            if (state.errors[errorName] != '')
           
            {
                valid = false;
            }
        }
        
        props.dispatch(userLoginAction(state.userLogin, props.history));
        
    }
    return (
        <div className="container">
            <h3>Login</h3>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <span>Account</span>
                    <input name="taiKhoan" className="form-control" onChange={handleChange} />
                    <span className="text text-danger">{state.errors.taiKhoan}</span>
                </div>
                <div className="form-group">
                    <span>Password</span>
                    <input name="matKhau" className="form-control" onChange={handleChange} />
                    <span className="text text-danger">{state.errors.matKhau}</span>
                </div>
                <div className="form-group">
                    <button type="submit" className="btn btn-success">Login</button>
                </div>
            </form>
        </div>
    )
}

export default connect()(Login)