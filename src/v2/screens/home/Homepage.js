import React, { Component } from 'react';
import FancyHeader from '../Layout/FancyHeader';
import Header from '../Layout/Header';
import Slider from '../Layout/Slider';
import CourseSlider from '../Layout/CourseSliders/CourseSlider';

class Homepage extends Component {
    render() {
        return (
            <>
            <FancyHeader/>
            <Header/>
            <Slider/>
            <CourseSlider/>
            </>
        )
    }
}
export default Homepage;
