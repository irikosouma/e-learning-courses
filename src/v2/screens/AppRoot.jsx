import React, { Component } from 'react';
import FancyHeader from '../components/FancyHeader';
import Header from '../components/Header';
import Slider from '../components/Slider';
import CourseSlider from './home/CourseSliders/CourseSlider';
import Number from '../components/Number';
export default class AppRoot extends Component {
  render() {
    return (
      <>
        <FancyHeader/>
        <Header/>
        <Number/>
        <Slider/>
        <CourseSlider/>  
      </>
    )
  }
}
