import React, { Component } from 'react'

class HeaderUs extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
                    <div className="navbar nav-logo col-3">
                        <a href=""> <img src="/img/logo-new.png" style={{ width: '157px', height: '44px' }} hspace='95' /></a>
                    </div>
                    <div className="collapse navbar-collapse col-6" id="collapsibleNavId">
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item dropdown">
                                <a
                                    class="nav-link"
                                    href="#"
                                    id="navbarDropdown"
                                    role="button"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                >
                                    <img src="/img/Categories.svg" style={{ width: '96px', height: '18px' }}></img>
                                </a>
                                <div className="dropdown-menu" aria-labelledby="dropdownId">
                                    <a className="dropdown-item" href="#">Action</a>
                                    <a className="dropdown-item" href="#">Another action</a>
                                    <div className="dropdown-divider"></div>
                                    <a className="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <div className="input-group mb-3">
                                <input type="text" className="form-control" placeholder="search" />
                                <div className="input-group-prepend">
                                    {/* <span className="input-group-text bg-white"><i class="fa fa-search"></i></span> */}
                                </div>
                            </div>
                        </ul>
                    </div>
                    <div className="col-4">
                        <div className="row">
                        <a type="button"><img src="/img/shopingcart.svg" style={{ width: '24px', height: '24px' }} ></img>1</a>
                                <a><img src="/img/mycourse.svg" style={{ width: '103px', height: '24px' }} ></img>1</a>
                                <a><img src="/img/avatar.svg" style={{ width: '24px', height: '23px' }} ></img></a>           
                        </div>
                    </div>
                </nav>
     
        )
    }
}

export default HeaderUs
