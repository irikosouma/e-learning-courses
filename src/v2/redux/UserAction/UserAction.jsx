import { LOGIN } from "../ActionType";
import { reduxAction } from "../Action";
// import Axios from 'axios';
import { settings } from "../config/setting";
import restConnector from "../../services/index";

export const userLoginAction = (userLogin, history) => {
  return dispatch => {
    restConnector({
      method: "POST",
      url: "/api/quanlynguoidung/dangnhap",
      data: userLogin
    })
      .then(res => {
        localStorage.setItem(settings.userLogin, JSON.stringify(res.data));
        localStorage.setItem(settings.token, res.data.accessToken);
        dispatch(reduxAction(LOGIN, res.data));
        restConnector.defaults.headers["Authorizations"] =
          "Bearer" + res.data.accessToken;
        history.push("./");
      })
      .catch(error => {
        console.log(error.response.data);
      });
  };
};
