import { SIGN_UP, FETCH_COURSE_DETAIL } from "./ActionType";
import CourseService from "../services/courseService";

const courseService = new CourseService();

export const reduxAction = (type, payload) => {
    return {
        type: type,
        payload: payload
    };
};

export const fetchCourseDetail = (courseId) => {
    return dispatch => {
        courseService
            .fetchCourseDetail(courseId)
            .then(res => {
                dispatch(reduxAction(FETCH_COURSE_DETAIL, res.data));
            })
            .catch(err => {
                console.log(err);
            });
    };
};