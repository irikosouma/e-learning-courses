import React, { Component } from "react";
import { Formik, Form, Field, validateYupSchema, ErrorMessage } from "formik";
import * as yup from "yup";
const signupUserSchema = yup.object().shape({
  taiKhoan: yup.string().required("*Field is required"),
  matKhau: yup.string().required("*Field is required"),
  hoTen: yup.string().required("*Field is required"),
  soDT: yup.string().required("*Field is required"),
  maLoaiNguoiDung: yup.string().required("*Field is required"),
  email: yup
    .string()
    .required("*Field is required")
    .email("*Field is required")
});

export default class SignUp extends Component {
  constructor(props){
    super(props);
  }

  _handleSubmit = (values, propsFormik) => {
    console.log(values);
    
    //TODO makup api from server
    propsFormik.resetForm()

    this.refClose.click();
  };
  render() {
    const init = {
      taiKhoan: "",
      matKhau: "",
      hoTen: "",
      soDT: "",
      maLoaiNguoiDung: "",
      maNhom: "GP01",
      email: ""
    }
    return (
      <div
        className="modal fade"
        id="signup"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Sign Up and Start Learning!</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                ref={that => this.refClose = that}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <Formik
              initialValues={init}
              validationSchema={signupUserSchema}
              onSubmit={this._handleSubmit}
            >
              {formikProps => {
                return (
                  <Form onSubmit={formikProps.handleSubmit}>
                    <div className="modal-body">
                      <div className="form">
                        <div className="form-group">
                          <Field
                            className="form-control"
                            type="text"
                            name="taiKhoan"
                            placeholder="username"
                            onChange={formikProps.handleChange}
                          />
                          <ErrorMessage name="taiKhoan" />
                        </div>
                        <div className="form-group">
                          <Field
                            className="form-control"
                            type="password"
                            name="matKhau"
                            placeholder="password"
                            onChange={formikProps.handleChange}
                          />
                          <ErrorMessage name="matKhau" />
                          <span className="glyphicon fa fa-key"></span>
                        </div>
                        <div className="form-group">
                          <Field
                            className="form-control"
                            type="fullname"
                            name="hoTen"
                            placeholder="fullname"
                            onChange={formikProps.handleChange}
                          />
                          <ErrorMessage name="hoTen" />
                          <span className="glyphicon fa fa-key"></span>
                        </div>
                        <div className="form-group">
                          <Field
                            className="form-control"
                            type="phone"
                            name="soDT"
                            placeholder="phone"
                            onChange={formikProps.handleChange}
                          />
                          <ErrorMessage name="soDT" />
                        </div>

                        <div className="form-group">
                          <Field
                            className="form-control"
                            type="usercode"
                            name="maLoaiNguoiDung"
                            placeholder="usercode"
                            onChange={formikProps.handleChange}
                          />
                          <ErrorMessage name="maLoaiNguoiDung" />
                        </div>
                        <div className="form-group">
                          <Field
                            component="select"
                            className="form-control"
                            type="groupcode"
                            name="maNhom"
                            placeholder="groupcode"
                            onChange={formikProps.handleChange}
                          >
                            <option>Mã: GP01</option>
                            <option>Mã: GP02</option>
                            <option>Mã: GP03</option>
                            <option>Mã: GP04</option>
                            <option>Mã: GP05</option>
                            <option>Mã: GP06</option>
                            <option>Mã: GP07</option>
                            <option>Mã: GP08</option>
                            <option>Mã: GP09</option>
                            <option>Mã: GP10</option>
                          </Field>
                        </div>
                        <div className="form-group">
                          <Field
                            className="form-control"
                            type="email"
                            name="email"
                            placeholder="email"
                            onChange={formikProps.handleChange}
                          />
                          <ErrorMessage name="email" />
                        </div>
                      </div>
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" value="" /> Checkboxes are used
                          if you want the user to select any number
                        </label>
                      </div>
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" value="" /> Checkboxes are used
                          if you want to choose User
                        </label>
                      </div>
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" value="" /> Checkboxes are used
                          if you want to choose Teacher
                        </label>
                      </div>
                      <button
                        type="submit"
                        className="btn btn-danger"
                       
                        disabled={formikProps.isSubmitting}
                      >
                        Sign Up
                      </button>
                    </div>
                    <div className="modal-footer">
                      <p>Already has account</p>
                      <button
                        type="button"
                        className="btn btn-link"
                        data-dismiss="modal"
                      >
                        Log in
                      </button>
                      =
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}
