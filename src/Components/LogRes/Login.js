import React, { Component } from 'react'

class Login extends Component {
    // constructor(props){
    //     super(props)
    // }

    render() {
        return (
            <div className="modal fade" id="login" tabIndex={-1} role="dialog" aria-labelledby="loginID" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Login to your 2Telearning account</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <span>
                                <i className="fa fa-user-circle"></i>
                                <p className="text-center">Welcome back!</p>
                            </span>
                            <input type="password" className="form-control" id="password" placeholder="password"></input>
                            <button className="btn btn-danger">Login</button>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-link" data-dismiss="modal">Fogot password</button>
                            <p>Don't have account <button type="button" className="btn btn-link">Sign up</button></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Login;